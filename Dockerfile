FROM alpine:edge

RUN apk update

RUN set -xe \
    && apk add --no-cache build-base \
        shadow \
        curl \
        git \
        gmp-dev \
        go \
        libgmpxx \
        python \
        python-dev \
        tor \
    && curl -sSL https://bootstrap.pypa.io/get-pip.py | python \
    && pip install fteproxy \
        obfsproxy \
    && GOPATH=/tmp/obfs4 go get git.torproject.org/pluggable-transports/obfs4.git/obfs4proxy \
        && mv /tmp/obfs4/bin/obfs4proxy /usr/bin \
        && rm -rf /tmp/obfs4 \
    && GOPATH=/tmp/meek go get git.torproject.org/pluggable-transports/meek.git/meek-server \
        && mv /tmp/meek/bin/meek-server /usr/bin \
        && rm -rf /tmp/meek \
    && apk del build-base \
        curl \
        git \
        gmp-dev \
        go \
        python-dev

COPY ./torrc /etc/tor/torrc

RUN groupadd -g 1000 zek_tor_bridge \
    && useradd -r -u 1000 -g zek_tor_bridge zek_tor_bridge

USER zek_tor_bridge

WORKDIR /var/lib/tor

EXPOSE 9001 9080 9081 9082 9083

CMD ["/usr/bin/tor", "-f", "/etc/tor/torrc"]
